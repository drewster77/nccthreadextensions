import XCTest

import NCCThreadExtensionsTests

var tests = [XCTestCaseEntry]()
tests += NCCThreadExtensionsTests.allTests()
XCTMain(tests)
